import MapView from "@trailstash/map-view";
import "@trailstash/map-view/dist/index.css";
import TrailStash from "@trailstash/stylejs-opentrailstash";
import Americana from "@trailstash/stylejs-americana";
import OpenMapTiles from "@trailstash/stylejs-openmaptiles";
import maplibregl from "maplibre-gl";

const searchParams = new URLSearchParams(window.location.search.slice(1));

const root = window.location.origin + window.location.pathname;

let hillshade = true;
let contours = "dem";
if (searchParams.get("terrain") == 0) {
  hillshade = false;
  contours = "none";
}

let tileUrl = "https://tiles.trailstash.net/opentrailstash.json";
if (searchParams.has("tiles")) {
  tileUrl = searchParams.get("tiles");
}

const view = new MapView({
  map: {
    style: new TrailStash({
      tileUrl,
      hillshade,
      contours,
      sprite: `${root}sprites/opentrailstash`,
      glyphs: "https://trailstash.github.io/openmaptiles-fonts/fonts/{fontstack}/{range}.pbf",
    }),
    container: "map",
    zoom: 14,
    center: [-77.46679, 37.53115],
    hash: true,
  },
  controls: [
    { type: "NavigationControl" },
    {
      type: "GeolocateControl",
      options: {
        positionOptions: {
          enableHighAccuracy: true,
        },
        trackUserLocation: true,
      },
    },
    {
      type: "FontAwesomeLinkControl",
      options: [
        "https://gitlab.com/trailstash/openstyle#opentrailstash",
        "About",
        "info",
      ],
    },
    { type: "MapSwapControl" },
    {
      position: "top-left",
      type: "OverpassSearch",
      options: {
        font: "Open Sans Bold",
        categories: [
          {
            name: "BMX/MTB Parks",
            query: `[bbox:{{bbox}}];
            (
              nwr[sport][leisure][sport~"(^|;)(bmx|mtb)(;|$)"];
              nwr[sport][leisure][name][sport~"(^|;)cycling(;|$)"][name~bmx,i];
            );
            out center;`,
            icon: "./dist/icons/jumps.png",
          },
          {
            name: "Skate Parks",
            query: `[bbox:{{bbox}}];nwr[sport=skateboard][!shop];out center;`,
            icon: "./dist/icons/skate_park.png",
          },
          {
            name: "Bike Repair Stations",
            query: `[bbox:{{bbox}}];node[amenity=bicycle_repair_station];out geom;`,
            icon: "./dist/icons/bike_repair_station.png",
          },
          {
            name: "Bike Shares",
            query: `[bbox:{{bbox}}];nwr[amenity=bicycle_rental][!shop][bicycle_rental!=shop];out center;`,
            icon: "./dist/icons/bike_share.png",
          },
          {
            name: "Toilets",
            query: `[bbox:{{bbox}}];(nwr[toilets=yes];nwr[amenity=toilets];);out center;`,
            icon: "./dist/icons/toilets.png",
          },
          {
            name: "Drinking Water",
            query: `[bbox:{{bbox}}];nwr[amenity=drinking_water];out center;`,
            icon: "./dist/icons/drinking_water.png",
          },
          {
            name: "Bike Shops",
            query: `[bbox:{{bbox}}];nwr[shop=bicycle];out center;`,
            icon: "./dist/icons/bike_shop.png",
          },
          {
            name: "Outfitters",
            query: `[bbox:{{bbox}}];nwr[shop=outdoor];out center;`,
            icon: "./dist/icons/outfitter.png",
          },
          {
            name: "Convenience Stores",
            query: `[bbox:{{bbox}}];nwr[shop=convenience];out center;`,
            icon: "./dist/icons/convenience_store.png",
          },
          {
            name: "Cafés",
            query: `[bbox:{{bbox}}];nwr[amenity=cafe];out center;`,
            icon: "./dist/icons/cafe.png",
          },
          {
            name: "Restaurants",
            query: `[bbox:{{bbox}}];nwr[amenity=restaurant];out center;`,
            icon: "./dist/icons/restaurant.png",
          },
          {
            name: "Fast Food",
            query: `[bbox:{{bbox}}];nwr[amenity=fast_food];out center;`,
            icon: "./dist/icons/fast_food.png",
          },
        ],
      },
    },
    {
      type: "ScaleControl",
      options: {
        unit: "imperial",
      },
    },
  ],
  PersistView: true,
  InitializeViewFromIP: {
    IpInfoToken: "a6134fb16df81b",
  },
});

// register service worker for PWA
if ("serviceWorker" in navigator) {
  navigator.serviceWorker.register("./sw.js").then(function (registration) {
    registration.update();
    console.log("Service Worker Registered");
  });
}
