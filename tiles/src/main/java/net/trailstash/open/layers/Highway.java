package net.trailstash.open.layers;

import static com.onthegomap.planetiler.util.Parse.parseIntOrNull;
import static com.protomaps.basemap.postprocess.LinkSimplify.linkSimplify;
import static java.util.Map.entry;

import com.onthegomap.planetiler.FeatureCollector;
import com.onthegomap.planetiler.FeatureMerge;
import com.onthegomap.planetiler.ForwardingProfile;
import com.onthegomap.planetiler.VectorTile;
import com.onthegomap.planetiler.geo.GeometryException;
import com.onthegomap.planetiler.reader.SourceFeature;
import com.protomaps.basemap.feature.FeatureId;
import java.util.*;

public class Highway implements ForwardingProfile.FeatureProcessor, ForwardingProfile.FeaturePostProcessor {
  // surface categories
  static Set<String> surfacePaved = Set.of(
    "paved",
    "asphalt",
    "cobblestone",
    "concrete",
    "concrete:lanes",
    "concrete:plates",
    "metal",
    "paving_stones",
    "sett",
    "unhewn_cobblestone",
    "wood"
  );
  static Set<String> surfaceUnimproved = Set.of(
    "unpaved",
    "dirt",
    "earth",
    "grass",
    "ground",
    "ice",
    "mud",
    "salt",
    "sand",
    "snow"
  );
  static Set<String> surfaceImproved = Set.of(
    "pebblestone",
    "compacted",
    "fine_gravel",
    "gravel",
    "gravel_turf",
    "woodchips",
    "grass_paver"
  );

  // highway categories for more complex categorization
  static Set<String> minorLike =
    Set.of("residential", "track", "service", "unclassified", "road", "living_street", "raceway");
  static Set<String> pathLike = Set.of("path", "cycleway", "bridleway", "footway");
  static Set<String> golfPathLike = Set.of("path", "track", "service", "footway");
  // highway categories for sipmle categorization
  static Map<String, String> highwayKinds = Map.ofEntries(
    entry("motorway", "freeway"),
    entry("motorway_link", "freeway"),
    entry("trunk", "highway"),
    entry("trunk_link", "highway"),
    entry("primary", "highway"),
    entry("primary_link", "highway"),
    entry("secondary", "major"),
    entry("secondary_link", "major"),
    entry("tertiary", "major"),
    entry("tertiary_link", "major")
  );

  // minzooms, for highway types for simple mappings(and more complex zoom logic)
  static Map<String, Integer> highwayMinZooms = Map.ofEntries(
    entry("motorway", 5),
    entry("motorway_link", 5),
    entry("trunk", 6),
    entry("trunk_link", 7),
    entry("primary", 7),
    entry("primary_link", 7),
    entry("secondary", 9),
    entry("secondary_link", 9),
    entry("tertiary", 9),
    entry("tertiary_link", 9)
  );
  static Map<String, Integer> highwayNameMinZooms = Map.ofEntries(
    entry("motorway", 7),
    entry("motorway_link", 12),
    entry("trunk", 12),
    entry("trunk_link", 13),
    entry("primary", 12),
    entry("primary_link", 12),
    entry("secondary", 12),
    entry("secondary_link", 14),
    entry("tertiary", 13),
    entry("tertiary_link", 14)
  );
  // minzooms, simple zoom logic for complex kind logic
  static Map<String, Integer> kindMinZooms = Map.ofEntries(
    entry("minor", 11),
    entry("service", 13),
    entry("track", 11),
    entry("path", 11),
    entry("trail", 11),
    entry("steps", 11),
    entry("sidewalk", 15),
    entry("cut", 13)
  );
  static Map<String, Integer> kindNameMinZooms = Map.ofEntries(
    entry("minor", 11),
    entry("service", 13),
    entry("track", 11),
    entry("path", 11),
    entry("trail", 11),
    entry("steps", 11),
    entry("sidewalk", 15),
    entry("cut", 14)
  );

  @Override
  public String name() {
    return "highway";
  }

  @Override
  public void processFeature(SourceFeature sf, FeatureCollector features) {
    if (sf.canBeLine() && sf.hasTag("highway") && (!sf.hasTag("indoor") || sf.hasTag("indoor", "no"))) {
      String kind = "";
      String name = sf.getString("name");
      if (name == null) {
        name = "";
      }
      String highway = sf.getString("highway");
      String surface = sf.getString("surface");
      if (surface == null) {
        surface = "";
      }

      String ref = sf.getString("ref");
      ref = (ref == null ? "" : ref.split(";")[0]);
      if (highway.equals("motorway") || highway.equals("motorway_link")) {
        name = ref;
      } else if (name.isEmpty()) {
        name = ref;
      }

      if (this.highwayKinds.containsKey(highway)) {
        kind = this.highwayKinds.get(highway);
      } else if ((sf.hasTag("golf", "path", "cart_path") || sf.hasTag("service", "cart_path") ||
        sf.hasTag("golf_cart", "yes", "designated")) && this.golfPathLike.contains(highway)) {
        name = "";
        // if just golf_cart=yes & highway=service, probably keep as service rd
        if (highway.equals("service") && !sf.hasTag("golf") && !sf.hasTag("service", "cart_path") &&
          sf.hasTag("golf_cart", "yes")) {
          kind = "service";
        } else {
          kind = "path";
        }
      } else if (minorLike.contains(highway)) {
        if (highway.equals("track")) {
          if (sf.hasTag("informal", "yes")) {
            kind = "cut";
          } else if (sf.hasTag("tracktype", "grade1", "grade2")) {
            if (sf.hasTag("motor_vehicle", "no")) {
              kind = "path";
            } else {
              kind = "minor";
            }
          } else if (this.surfacePaved.contains(surface) ||
            sf.hasTag("smoothness", "good", "excellent", "intermediate")) {
            kind = "minor";
          } else {
            kind = "track";
          }
          if (surface.isEmpty()) {
            if (sf.hasTag("tracktype", "grade1")) {
              surface = "paved";
            } else if (sf.hasTag("tracktype", "grade2")) {
              surface = "gravel";
            } else if (sf.hasTag("tracktype", "grade3", "grade4", "grade5")) {
              surface = "unpaved";
            }
          }
        } else if (sf.hasTag("smoothness", "very_bad", "horrible", "very_horrible")) {
          kind = "track";
        } else if (highway.equals("service") && sf.hasTag("service")) {
          kind = "service";
        } else {
          kind = "minor";
        }
      } else if (highway.equals("steps")) {
        kind = "steps";
      } else if (highway.equals("pedestrian")) {
        kind = "path";
      } else if (this.pathLike.contains(highway)) {
        if (sf.hasTag("informal", "yes") || sf.hasTag("smoothness", "impassable")) {
          kind = "cut";
        } else if (sf.hasTag("footway", "sidewalk", "crossing", "residential", "alley", "access_aisle", "traffic_island", "lane")) {
          kind = "sidewalk";
        } else if (sf.hasTag("footway", "link", "indoor")) {
          return;
        } else if (sf.hasTag("mtb:scale", "1", "2", "3", "4", "5", "6") ||
          sf.hasTag("mtb:scale:imba", "1", "2", "3", "4") ||
          sf.hasTag("sac_scale")) {
          kind = "trail";
        } else if (surface.isEmpty()) {
          if (highway.equals("path")) {
            kind = "trail";
            surface = "unpaved";
          } else {
            kind = "path";
            surface = "paved";
          }
        } else if (this.surfacePaved.contains(surface)) {
          kind = "path";
        } else if (this.surfaceUnimproved.contains(surface)) {
          kind = "trail";
        } else {
          kind = "path";
        }
      } else {
        // Ignore unknown highway tags
        return;
      }

      int minZoom = 99;
      if (this.highwayMinZooms.containsKey(highway)) {
        minZoom = this.highwayMinZooms.get(highway);
      } else if (this.kindMinZooms.containsKey(kind)) {
        minZoom = this.kindMinZooms.get(kind);
      }
      var feature = features.line(this.name())
        .setId(FeatureId.create(sf))
        // Core Tilezen schema properties
        .setAttr("type", kind)
        // To power better client label collisions
        //.setAttr("minzoom", minZoom)
        // Core OSM tags for different kinds of places
        //.setAttrWithMinzoom("oneway", sf.getString("oneway"), 14)
        // DEPRECATION WARNING: Marked for deprecation in v4 schema, do not use these for styling
        //                      If an explicate value is needed it should bea kind, or included in kind_detail
        .setAttr("highway", highway)
        .setMinPixelSize(0)
        .setPixelTolerance(0)
        .setZoomRange(minZoom, 16);

      // unpaved
      if (this.surfaceUnimproved.contains(surface) || this.surfaceImproved.contains(surface)) {
        feature.setAttr("unpaved", true);
      }

      //access
      if (sf.hasTag("access", "private", "no") && !sf.hasTag("foot") && !sf.hasTag("horse") && !sf.hasTag("bicycle")) {
        feature.setAttr("access", sf.getTag("access"));
      }

      // bike
      if (sf.hasTag("highway", "cycleway")) {
        feature.setAttr("bike", "designated");
      } else if (sf.hasTag("bicycle")) {
        feature.setAttr("bike", sf.getString("bicycle"));
      }

      // cycleways
      if (sf.hasTag("cycleway", "track") || sf.hasTag("cycleway:left", "track") ||
        sf.hasTag("cycleway:right", "track")) {
        feature.setAttr("cycleway", "cycletrack");
      } else if (sf.hasTag("cycleway", "lane") || sf.hasTag("cycleway:left", "lane") ||
        sf.hasTag("cycleway:right", "lane")) {
        feature.setAttr("cycleway", "bikelane");
      } else if (sf.hasTag("cycleway", "shared_lane") || sf.hasTag("cycleway:left", "shared_lane") ||
        sf.hasTag("cycleway:right", "shared_lane")) {
        feature.setAttr("cycleway", "sharrows");
      }

      // link
      if (highway.endsWith("_link")) {
        feature.setAttr("link", true);
      }

      // layer
      var layer = parseIntOrNull(sf.getString("layer"));
      if (sf.hasTag("bridge") && !sf.hasTag("bridge", "no")) {
        feature.setAttrWithMinzoom("layer", layer != null ? layer : 1, 12);
      } else if (sf.hasTag("tunnel") && !sf.hasTag("tunnel", "no")) {
        feature.setAttrWithMinzoom("layer", layer != null ? layer : -1, 12);
      }

      // Server sort features so client label collisions are pre-sorted
      feature.setSortKey(minZoom);

      // name
      if (!name.isEmpty()) {
        int minZoomNames = 99;
        if (this.highwayNameMinZooms.containsKey(highway)) {
          minZoomNames = this.highwayNameMinZooms.get(highway);
        } else if (this.kindNameMinZooms.containsKey(kind)) {
          minZoomNames = this.kindNameMinZooms.get(kind);
        }

        feature.setAttrWithMinzoom("name", name, minZoomNames);
      }
    }
  }

  @Override
  public List<VectorTile.Feature> postProcess(int zoom, List<VectorTile.Feature> items) throws GeometryException {
    // limit the application of LinkSimplify to where cloverleafs are unlikely to be at tile edges.
    // TODO: selectively apply each class depending on zoom level.
    if (zoom < 12) {
      items = linkSimplify(items, "highway", "motorway", "motorway_link");
      items = linkSimplify(items, "highway", "trunk", "trunk_link");
      items = linkSimplify(items, "highway", "primary", "primary_link");
      items = linkSimplify(items, "highway", "secondary", "secondary_link");
    }

    for (var item : items) {
      item.attrs().remove("highway");
    }

    items = FeatureMerge.mergeLineStrings(items,
      0.5, // after merging, remove lines that are still less than 0.5px long
      0.1, // simplify output linestrings using a 0.1px tolerance
      4 // remove any detail more than 4px outside the tile boundary
    );

    return items;
  }
}
