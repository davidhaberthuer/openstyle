package net.trailstash.open.layers;

import static com.onthegomap.planetiler.util.Parse.parseIntOrNull;
import static com.protomaps.basemap.postprocess.LinkSimplify.linkSimplify;
import static java.util.Map.entry;

import com.onthegomap.planetiler.FeatureCollector;
import com.onthegomap.planetiler.FeatureMerge;
import com.onthegomap.planetiler.ForwardingProfile;
import com.onthegomap.planetiler.VectorTile;
import com.onthegomap.planetiler.geo.GeometryException;
import com.onthegomap.planetiler.reader.SourceFeature;
import com.protomaps.basemap.feature.FeatureId;
import java.util.*;

public class ManMade implements ForwardingProfile.FeatureProcessor, ForwardingProfile.FeaturePostProcessor {
  @Override
  public String name() {
    return "manmade";
  }

  @Override
  public void processFeature(SourceFeature sf, FeatureCollector features) {
    if (sf.canBeLine() && (sf.hasTag("power") || sf.hasTag("man_made") || sf.hasTag("barrier"))) {
      String kind = "";
      String kindDetail = "";
      String name = "";
      int minZoom = 99;
      if (sf.hasTag("name")) {
        name = sf.getString("name");
      }
      String power = sf.getString("power");
      if (sf.hasTag("power", "line")) {
          kind = "power_line";
          minZoom = 13;
      } else if (sf.hasTag("power", "minor_line")) {
          kind = "minor_power_line";
          minZoom = 15;
      } else if (sf.hasTag("man_made", "pipeline")) {
          kind = "pipeline";
          minZoom = 13;
          if (sf.hasTag("location", "underground", "underwater") || (!sf.hasTag("location") && sf.hasTag("layer", "-1"))) {
              kindDetail = "underground";
          } else {
              kindDetail = "aboveground";
          }
      } else if (sf.hasTag("barrier", "wall")) {
          kind = "barrier";
          kindDetail = "wall";
          minZoom = 15;
      } else if (sf.hasTag("barrier", "fence")) {
          kind = "barrier";
          kindDetail = "fence";
          minZoom = 15;
      }

      if (minZoom < 99) {
          var feature = features.line(this.name())
            .setId(FeatureId.create(sf))
            .setAttr("kind", kind)
            .setMinPixelSize(0)
            .setPixelTolerance(0)
            .setZoomRange(minZoom, 16);
          if (kindDetail != "") {
              feature.setAttr("kind:detail", kindDetail);
          }
      }
    }
    if (sf.isPoint() || sf.hasTag("power")) {
      String kind = "";
      String name = "";
      int minZoom = 99;
      if (sf.hasTag("name")) {
        name = sf.getString("name");
      }
      String power = sf.getString("power");
      if (sf.hasTag("power", "tower")) {
          kind = "tower";
          minZoom = 13;
      }
      if (sf.hasTag("power", "pole")) {
          kind = "pole";
          minZoom = 15;
      }

      if (minZoom < 99) {
          var feature = features.point(this.name())
            .setId(FeatureId.create(sf))
            .setAttr("kind", kind)
            .setZoomRange(minZoom, 16);
      }
    }
  }

  @Override
  public List<VectorTile.Feature> postProcess(int zoom, List<VectorTile.Feature> items) throws GeometryException {
    return items;
  }
}
