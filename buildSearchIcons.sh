#!/bin/bash

set -e

rm -rf static/dist/icons
mkdir -p static/dist/icons

for file in style/sprite-src/iconset/svgs/*; do
    out=${file%.svg*}
    out=${out##*/}
    out=static/dist/icons/$out.png
    inkscape -w 64 -h 64 $file -o $out
done
