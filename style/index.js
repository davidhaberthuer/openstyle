import mlcontour from "maplibre-contour/dist/index.mjs";
import * as pmtiles from "pmtiles";
import {
  transpoLabelLayers,
  tunnelTranspoLayers,
  bridgeTranspoLayers,
  surfaceTranspoLayers,
} from "./layers/transpo.js";
import { powerLayers, undergroundPipelinesLayer, pipelinesLayer, barrierLayer } from "./layers/manmade.js";
import { naturalEarthShadedReliefLayer } from "./layers/naturalearth.js";
import { waterLayers, waterLabelLayers } from "./layers/water.js";
import * as landcoverLayers from "./layers/landcover.js";
import * as landuseLayers from "./layers/landuse.js";
import { contourLayers, maptilerContourLayers } from "./layers/contours.js";
import * as poiLayers from "./layers/poi.js";
import { hillshadeLayer } from "./layers/hillshade.js";
import { boundariesLayers } from "./layers/boundaries.js";
import { placesLayers } from "./layers/places.js";
import buildingsLayer from "./layers/buildings.js";

export class TrailStash {
  constructor({
    sprite,
    glyphs,
    tileUrl,
    hillshade = true,
    contours = "dem",
    maptilerKey,
  }) {
    this.sprite = sprite;
    this.glyphs = glyphs;
    this.tileUrl = tileUrl;
    this.hillshade = hillshade;
    this.contours = contours;
    this.maptilerKey = maptilerKey;
  }
  setupMapLibreGL(maplibregl) {
    if (!this.demSource && this.contours == "dem") {
      this.demSource = new mlcontour.DemSource({
        url: "https://s3.amazonaws.com/elevation-tiles-prod/terrarium/{z}/{x}/{y}.png",
        encoding: "terrarium",
        maxzoom: 15,
        worker: true,
        cacheSize: 100,
        timeoutMs: 10_000,
      });
      this.demSource.setupMaplibre(maplibregl);
    }
    if (this.tileUrl.startsWith("pmtiles://")) {
      let protocol = new pmtiles.Protocol();
      maplibregl.addProtocol("pmtiles",protocol.tile);
    }
  }
  teardown(map, maplibregl) {
    maplibregl.removeProtocol("pmtiles");
  }
  build() {
    let contourSource;
    if (this.demSource && this.contours == "dem") {
      contourSource = {
        type: "vector",
        tiles: [
          this.demSource.contourProtocolUrl({
            // convert meters to feet, default=1 for meters
            multiplier: 3.28084,
            thresholds: {
              // zoom: [minor, major]
              11: [200, 1000],
              12: [100, 500],
              14: [50, 200],
              15: [20, 100],
            },
            contourLayer: "contour",
            elevationKey: "ele",
            levelKey: "level",
            extent: 4096,
            buffer: 1,
          }),
        ],
      };
    } else if (this.contours == "maptiler") {
      contourSource = {
        type: "vector",
        url: `https://api.maptiler.com/tiles/contours/tiles.json?key=${this.maptilerKey}`,
      };
    }
    let terrariumSource;
    if (this.hillshade) {
      if (this.demSource) {
        terrariumSource = {
          type: "raster-dem",
          tiles: [this.demSource.sharedDemProtocolUrl],
          minzoom: 0,
          maxzoom: 15,
          tileSize: 256,
          encoding: "terrarium",
        };
      } else {
        terrariumSource = {
          type: "raster-dem",
          tiles: [
            "https://s3.amazonaws.com/elevation-tiles-prod/terrarium/{z}/{x}/{y}.png",
          ],
          minzoom: 0,
          maxzoom: 15,
          tileSize: 256,
          encoding: "terrarium",
        };
      }
    }
    return {
      id: "opentrailstash",
      version: 8,
      name: "OpenTrailStash",
      sources: {
        trailstash: {
          type: "vector",
          attribution: `<a href="https://www.openstreetmap.org/copyright" target="_blank">© OpenStreetMap contributors</a>`,
          url: this.tileUrl,
        },
        natural_earth_shaded_relief: {
          maxzoom: 6,
          tileSize: 256,
          tiles: [
            "https://trailstash.github.io/naturalearthtiles/tiles/natural_earth_2_shaded_relief.raster/{z}/{x}/{y}.png",
          ],
          type: "raster",
        },
        ...(contourSource ? { contours: contourSource } : {}),
        ...(terrariumSource ? { terrarium: terrariumSource } : {}),
      },
      sprite: this.sprite,
      glyphs: this.glyphs,
      layers: [
        {
          id: "background",
          type: "background",
          layout: {
            visibility: "visible",
          },
          paint: {
            "background-color": "rgb(239,239,239)",
          },
        },
        naturalEarthShadedReliefLayer,
        landuseLayers.park,
        landcoverLayers.green,
        landcoverLayers.wetland,
        landcoverLayers.glacier,
        landcoverLayers.bare_rock,
        // residential
        // industrial
        landuseLayers.cemetery,
        landuseLayers.golf,
        // hospital
        landuseLayers.school,
        // quarry,
        // stadium,
        // track(landuse),
        landuseLayers.pitch,
        landuseLayers.playground,
        landcoverLayers.sand,
        ...(terrariumSource ? [hillshadeLayer] : []),
        ...(contourSource
          ? this.contours == "dem"
            ? contourLayers
            : maptilerContourLayers
          : []),
        landuseLayers.beach,
        ...waterLayers,
        landuseLayers.aerodrome,
        undergroundPipelinesLayer,
        // wilderness/
        ...landuseLayers.parkOutline,
        landuseLayers.military,
        ...tunnelTranspoLayers,
        barrierLayer,
        ...surfaceTranspoLayers,
        pipelinesLayer,
        buildingsLayer,
        ...bridgeTranspoLayers,
        ...transpoLabelLayers,
        ...powerLayers,
        ...boundariesLayers,
        ...waterLabelLayers,
        poiLayers.dot,
        ...placesLayers,
        poiLayers.poi,
      ],
    };
  }
}

export default TrailStash;
