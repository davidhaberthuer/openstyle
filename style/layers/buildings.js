export default {
  id: "buildings",
  type: "fill",
  source: "trailstash",
  "source-layer": "buildings",
  minzoom: 13,
  paint: {
    "fill-color": "hsl(36, 7%, 70%)",
    "fill-opacity": 0.5,
  },
};
