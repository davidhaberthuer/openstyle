export const naturalEarthShadedReliefLayer = {
  id: "natural_earth",
  type: "raster",
  source: "natural_earth_shaded_relief",
  maxzoom: 6,
  layout: {
    visibility: "visible",
  },
  paint: {
    "raster-opacity": {
      base: 1.5,
      stops: [
        [0, 0.6],
        [6, 0.1],
      ],
    },
  },
};
