import Values from "values.js";

const categoryLabelColor = [
  "case",
  ["==", ["get", "category"], "transportation"], new Values("#6486f5").shade(30).hexString(),
  ["==", ["get", "category"], "food"], new Values("#ed7c26").shade(30).hexString(),
  ["==", ["get", "category"], "shop"], new Values("#7e53f5").shade(30).hexString(),
  ["==", ["get", "category"], "water"], new Values("#82aaff").shade(30).hexString(),
  ["==", ["get", "category"], "red"], new Values("#ff0000").shade(30).hexString(),
  ["==", ["get", "category"], "attraction"], new Values("#6dad6f").shade(30).hexString(),
  "#6e4b4b"
];
  
const categoryColor = [
  "case",
  ["==", ["get", "category"], "transportation"], "#6486f5",
  ["==", ["get", "category"], "food"], "#ed7c26",
  ["==", ["get", "category"], "shop"], "#7e53f5",
  ["==", ["get", "category"], "water"], "#82aaff",
  ["==", ["get", "category"], "red"], "#ff0000",
  ["==", ["get", "category"], "attraction"], "#6dad6f",
  "#6e4b4b"
];
export const poi = {
  id: "poi",
  type: "symbol",
  source: "trailstash",
  "source-layer": "poi",
  filter: [">=", ["zoom"], ["number", ["get", "pmap:min_zoom"]]],
  layout: {
    "icon-padding": [
      "case",
      ["in", ["get", "pmap:kind"], ["literal", [
        "bench",
        "gate",
        "trash_bin",
        "recycling",
        "picnic_table",
      ]]], -2,
      2
    ],
    "symbol-sort-key": ["get", "order"],
    "text-field": "{name}",
    "icon-image": ["get", "pmap:kind"],
    "text-optional": true,
    "text-size": 12,
    'text-radial-offset': .9,
    'text-variable-anchor': ["left", "right", "top", "bottom"],
    'text-justify': 'auto',
    "text-font": ["Open Sans Italic"],
    "text-max-width": 6,
    "symbol-z-order": "source",
  },
  paint: {
    "text-color": categoryLabelColor,
    "text-halo-blur": 0.5,
    "text-halo-color": "#ffffff",
    "text-halo-width": 2,
  },
};
export const dot = {
  id: "poi_dot",
  type: "circle",
  source: "trailstash",
  "source-layer": "poi",
  filter: [
    "all",
    [">=", ["zoom"], ["number", ["get", "pmap:min_zoom"]]],
    ["!", ["in", ["get", "pmap:kind"], ["literal", ["bench", "picnic_table"]]]],
  ],
  paint: {
    "circle-color": categoryColor,
    "circle-radius": {stops: [[13.99, 0], [14, 1.5], [15, 1.8], [16, 2.5], [17, 3]]},
    "circle-stroke-width": 1,
    "circle-stroke-color": "white",
  },
  //minzoom: 16,
};
