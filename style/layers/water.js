import compose from "./compose.js";
import * as colors from "../colors.js";
import { and, eq, get, not } from "../filters.js";

const styleWaterbody = {
  type: "fill",
  paint: {
    "fill-color": colors.water,
    //"fill-outline-color": colors.waterLabel,
  },
};
const styleWaterbodyShadow = (layer) => ({
  ...layer,
  id: `${layer.id}_shadow`,
  type: "line",
  layout: {
    "line-join": "round",
  },
  paint: {
    "line-color": colors.waterOutline,
    "line-offset": 2,
    "line-width": 2,
    "line-opacity": {
      stops: [
        [10, 0],
        [15, 1],
      ],
    },
  },
});
const styleWateray = {
  type: "line",
  paint: {
    "line-color": colors.water,
    "line-width": {
      stops: [
        [9, 1],
        [16, 2],
        [20, 6],
      ],
    },
    "line-opacity": {
      stops: [
        [9, 0.7],
        [13, 1],
      ],
    },
  },
};

const styleModIntermittentWaterbody = { paint: { "fill-opacity": 0.3333 } };
const styleModIntermittentWaterway = { paint: { "line-dasharray": [1, 1] } };

const layer = (name) => {
  switch (name) {
    case "waterbody":
      return {
        id: "water",
        source: "trailstash",
        "source-layer": "water",
        filter: eq(["geometry-type"], "Polygon"),
      };
    case "waterway":
      return {
        id: "water_way",
        source: "trailstash",
        "source-layer": "water",
        minzoom: 9,
        filter: eq(["geometry-type"], "LineString"),
      };
    case "waterbody_label":
      return {
        id: "water_label",
        source: "trailstash",
        "source-layer": "water",
        filter: eq(["geometry-type"], "Point"),
      };
    case "waterway_label":
      return {
        id: "water_way_label",
        source: "trailstash",
        "source-layer": "water",
        minzoom: 14,
        filter: eq(["geometry-type"], "LineString"),
      };
  }
};

const isNotIntermittent = (layer) => ({
  ...layer,
  filter: and(layer.filter, not(eq(get("intermittent"), true))),
});
const isIntermittent = (layer) => ({
  ...layer,
  id: `${layer.id}_intermittent`,
  filter: and(layer.filter, eq(get("intermittent"), true)),
});

export const waterLayers = [
  compose(layer("waterbody"), isNotIntermittent, styleWaterbody),
  //compose(layer("waterbody"), isNotIntermittent, styleWaterbodyShadow),
  compose(
    layer("waterbody"),
    isIntermittent,
    styleWaterbody,
    styleModIntermittentWaterbody
  ),
  compose(layer("waterway"), isNotIntermittent, styleWateray),
  compose(
    layer("waterway"),
    isIntermittent,
    styleWateray,
    styleModIntermittentWaterway
  ),
];

const styleLabelText = {
  type: "symbol",
  layout: {
    "text-field": "{name}",
    "text-font": ["Open Sans Semibold Italic"],
    "text-size": 12,
  },
  paint: {
    "text-color": colors.waterLabel,
    "text-halo-color": colors.waterLabelHalo,
    "text-halo-width": 1,
  },
};
const styleModWaterbodyLabelLayout = {
  layout: {
    "text-max-width": 5,
  },
};
const styleModWaterwayLabelLayout = {
  layout: {
    "text-max-width": 10,
    "symbol-placement": "line",
    "symbol-spacing": 350,
    "text-padding": 5,
  },
};

export const waterLabelLayers = [
  compose(
    layer("waterbody_label"),
    styleLabelText,
    styleModWaterbodyLabelLayout
  ),
  compose(layer("waterway_label"), styleLabelText, styleModWaterwayLabelLayout),
];
