import { writeFileSync } from "fs";
import TrailStash from "./index.js";

const [root, tileUrl, hillshade, contours, maptilerKey] = process.argv.slice(2);
const style = new TrailStash({
  sprite: `${root}sprites/opentrailstash`,
  glyphs: `${root}fonts/{fontstack}/{range}.pbf`,
  tileUrl,
  hillshade,
  contours,
  maptilerKey,
});
writeFileSync("style.json", JSON.stringify(style.build()));
