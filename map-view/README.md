# TrailStash MapView

An opinionated configuration-driven map viewer powered by MapLibre GL JS.

A basic example comparing plain MapLibre GL JS usage and via TrailStash MapView:

```javascript
// MapLibre GL JS
import maplibregl from "maplibre-gl.js";

const map = new maplibregl.Map({
  container: "map",
  style: "https://overpass-ultra.trailsta.sh/style.json",
  zoom: 4,
  center: [-100, 40],
  attributionControl: false,
});
map.addControl(new NavigationControl());
map.addControl(
  new GeolocateControl({
    positionOptions: {
      enableHighAccuracy: true,
    },
    trackUserLocation: true,
  }),
  "bottom-right",
);

// TrailStash MapView
import MapView from "@trailstash/map-view";
const mapView = new MapView({
  map: {
    container: "map",
    style: "https://overpass-ultra.trailsta.sh/style.json",
    zoom: 4,
    center: [-100, 40],
    attributionControl: false,
  },
  controls: [
    { type: "NavigationControl" },
    {
      position: "bottom-right",
      type: "GeolocateControl",
      options: {
        positionOptions: {
          enableHighAccuracy: true,
        },
        trackUserLocation: true,
      },
    },
  ],
});
```

In early stages of development and derived from [OpenBikeHero](https://gitlab.com/trailstash/openbikehero).

## Hosted

There is a hosted version of the viewer at https://trailstash.gitlab.io/map-view/

It can load JS or JSON configs passed to the `config` search parameter.

EG: https://trailstash.gitlab.io/map-view/?config=./configs/minimal.js

## Configs:

| link                                                                                                      | path                                                                                                                         | description                                                                             | works on hosted version |
| --------------------------------------------------------------------------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- | --------------------------------------------------------------------------------------- | ----------------------- |
| http://localhost:8000/?config=./configs/dev.js                                                            | [`./configs/dev.js`](./configs/dev.js)                                                                                       | A few controls & OSM OpenMapTiles style using Americana tile server(only works locally) | :x:                     |
| https://trailstash.gitlab.io/map-view/?config=./configs/minimal.js                                        | [`./configs/minimal.js`](./configs/minimal.js)                                                                               | a minimal examle that loads OSM Carto raster tiles                                      | :heavy_check_mark:      |
| https://trailstash.gitlab.io/map-view/?config=./configs/demo.json                                         | [`./configs/demo.json`](./configs/demo.json)                                                                                 | The default configuration used at https://trailstash.gitlab.io/map-view/                | :heavy_check_mark:      |
| https://trailstash.gitlab.io/map-view/?config=./configs/opentrailstash.js                                 | [`./configs/opentrailstash.js`](./configs/opentrailstash.js)                                                                 | Identical to https://open.trailsta.sh except for the lack of the Overpass Ultra button  | :heavy_check_mark:      |
| https://trailstash.gitlab.io/map-view/?config=https://trailstash.gitlab.io/openstyle-v2/mapview-config.js | [`https://trailstash.gitlab.io/openstyle-v2/mapview-config.js`](https://trailstash.gitlab.io/openstyle-v2/mapview-config.js) | The OpenTrailStash v2 prototype                                                         | :heavy_check_mark:      |
