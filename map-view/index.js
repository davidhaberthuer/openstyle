import maplibregl from "maplibre-gl";
import "maplibre-gl/dist/maplibre-gl.css";
import FontAwesomeLinkControl from "./controls/FontAwesomeLinkControl.js";
import EditOnOsmControl from "./controls/EditOnOsmControl.js";
import MapSwapControl from "./controls/MapSwapControl.js";
import LayerControl from "./controls/LayerControl.js";
import AutoClosingAttributionControl from "./controls/AutoClosingAttributionControl.js";
import OverpassSearch from "./controls/OverpassSearch.js";

function getView() {
  var hash = localStorage.getItem("hash");
  var x = 0;
  var y = 0;
  var z = 0;
  if (hash) {
    [z, y, x] = hash.slice(1).split("/");
    return { zoom: z, center: [x, y] };
  }
}

export class MapView {
  constructor(config) {
    // Support JS-based styles
    this.style = MapView.ensureStyleJS(config.map.style);
    // get initial map view
    let { center, zoom } = config.map;
    const initialView = config.PersistView ? getView() : undefined;
    if (initialView) {
      ({ center, zoom } = initialView);
    } else if (config.InitializeViewFromIP) {
      fetch(
        `https://ipinfo.io/json?token=${config.InitializeViewFromIP.IpInfoToken}`
      )
        .then((r) => r.json())
        .then(({ loc }) => {
          let center = loc.split(",");
          center = center.reverse();
          map.flyTo({ center, zoom: 10 });
        });
    }
    // Support JS-based styles
    if (this.style.setupMapLibreGL instanceof Function) {
      this.style.setupMapLibreGL(maplibregl);
    }
    // initialize maplibregl map
    this.map = new maplibregl.Map({
      ...config.map,
      center,
      zoom,
      style: this.style.build(),
    });
    // Support JS-based styles
    if (this.style.setupMap instanceof Function) {
      this.style.setupMap(this.map);
    }
    // hook for persisting view
    if (config.PersistView) {
      this.map.on("moveend", () =>
        localStorage.setItem(
          "hash",
          `#${this.map.getZoom(0).toFixed(6)}/${this.map
            .getCenter()
            .lat.toFixed(6)}/${this.map.getCenter().lng.toFixed(6)}`
        )
      );
    }
    // add controls
    for (const { type, options, position } of config.controls || []) {
      let control;
      switch (type) {
        // Built-in MaplibreGL controls
        case "AttributionControl":
        case "ScaleControl":
        case "NavigationControl":
        case "GeolocateControl":
          control = new maplibregl[type](options || {});
          break;
        case "FontAwesomeLinkControl":
          control = new FontAwesomeLinkControl(...options);
          break;
        case "EditOnOsmControl":
          control = new EditOnOsmControl();
          break;
        case "MapSwapControl":
          control = new MapSwapControl(options || {});
          break;
        case "LayerControl":
          control = new LayerControl(options || {}, this);
          break;
        case "AutoClosingAttributionControl":
          control = new AutoClosingAttributionControl(options || {});
          break;
        case "OverpassSearch":
          control = new OverpassSearch(options || {});
          break;
      }
      if (control) {
        this.map.addControl(control, position);
      }
    }
  }
  static ensureStyleJS(style) {
    if (!style) {
      return { build: () => ({ version: 8, sources: {}, layers: [] }) };
    } else if (style.build instanceof Function) {
      return style;
    }
    return { build: () => style };
  }
  setStyle(style) {
    style = MapView.ensureStyleJS(style);
    if (this.style === style) {
      return; // TODO: test this. I dunno if this check works..
    }

    if (this.style.teardown instanceof Function) {
      this.style.teardown(this.map, maplibregl);
    }
    this.style = style;
    if (this.style.setupMapLibreGL instanceof Function) {
      this.style.setupMapLibreGL(maplibregl);
    }
    this.map.setStyle(this.style.build());
    if (this.style.setupMap instanceof Function) {
      this.style.setupMap(this.map);
    }
  }
  static async loadConfig(configOrConfigURL) {
    let config;
    if (configOrConfigURL.endsWith(".js")) {
      ({ default: config } = await import(configOrConfigURL));
    } else if (configOrConfigURL.endsWith(".json")) {
      config = await (await fetch(configOrConfigURL)).json();
    } else {
      throw Error("MapView.LoadConfig can only load .js and .json files");
    }
    return config;
  }
}

export default MapView;
