import { AttributionControl } from "maplibre-gl";

export default class AutoClosingAttributionControl {
  constructor({ customAttribution, timeout = 5000 }) {
    this.timeout = timeout;
    this.control = new AttributionControl({
      compact: true,
      customAttribution: this.customAttribution,
    });
  }
  onAdd(map) {
    this.container = this.control.onAdd(map);
    this.timeoutId = setTimeout(() => {
      this.container.open = false;
      this.container.classList.remove("maplibregl-compact-show");
      this.timeoutId = undefined;
    }, this.timeout);
    return this.container;
  }
  remove(map) {
    if (this.timeoutId) {
      clearTimeout(this.timeoutId);
    }
    return this.control.remove(map);
  }
  getDefaultPosition() {
    return this.control.getDefaultPosition();
  }
}
