import "./LayerControl.css";

export default class LayerControl {
  constructor({ layers }, mapView) {
    this.mapView = mapView;
    this.layers = layers;
    this.current =
      window.localStorage.getItem("trailstash:layercontrol:currentLayer") || 0;
  }
  onAdd(map) {
    this.map = map;
    this.container = document.createElement("div");
    this.container.className = `trailstash-layercontrol maplibregl-ctrl`;
    this.container.addEventListener("contextmenu", (e) => e.preventDefault());

    for (const i in this.layers) {
      const button = document.createElement("button");
      button.dataset.styleIdx = i;
      button.title = this.layers[i].name;
      button.innerHTML = `<img src="${this.layers[i].icon}"/>`;
      if (i == this.current) {
        button.classList.add("selected");
        this.mapView.setStyle(this.layers[this.current].style);
      }
      button.addEventListener("click", (e) => {
        e.preventDefault();
        for (const b of this.container.children) {
          if (b === e.currentTarget) {
            this.current = e.currentTarget.dataset.styleIdx;
            window.localStorage.setItem(
              "trailstash:layercontrol:currentLayer",
              this.current
            );
            e.currentTarget.classList.add("selected");
            this.mapView.setStyle(this.layers[this.current].style);
          } else {
            b.classList.remove("selected");
          }
        }
        const c = this.container;
      });
      this.container.appendChild(button);
    }

    return this.container;
  }
  onRemove() {
    this.container.parentNode.removeChild(this.container);
    this.map = undefined;
  }
}
