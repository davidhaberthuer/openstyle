export default {
  map: {
    container: "map",
    zoom: 4,
    center: [-100, 40],
    attributionControl: false,
  },
  controls: [
    { type: "EditOnOsmControl" },
    {
      position: "bottom-left",
      type: "LayerControl",
      options: {
        layers: [
          {
            name: "OpenTrailStash",
            style: { build: () => "https://open.trailsta.sh/style.json" },
            icon: "./logo.png",
          },
          {
            name: "OSM OpenMapTiles",
            style: "https://overpass-ultra.trailsta.sh/style.json",
            icon: "./omt.png",
          },
        ],
      },
    },
    { type: "NavigationControl" },
    {
      position: "bottom-right",
      type: "GeolocateControl",
      options: {
        positionOptions: {
          enableHighAccuracy: true,
        },
        trackUserLocation: true,
      },
    },
    {
      type: "FontAwesomeLinkControl",
      options: [
        "https://gitlab.com/trailstash/map-view#trailstash-mapview",
        "About",
        "info",
      ],
    },
    {
      type: "ScaleControl",
      options: {
        unit: "imperial",
      },
    },
    {
      type: "AutoClosingAttributionControl",
      position: "bottom-left",
    },
  ],
  PersistView: true,
  InitializeViewFromIP: {
    IpInfoToken: "a6134fb16df81b",
  },
};
