export default {
  map: {
    container: "map",
    style: {
      version: 8,
      name: "OpenStreetMap",
      sources: {
        OpenStreetMap: {
          tiles: ["https://tile.openstreetmap.org/{z}/{x}/{y}.png"],
          type: "raster",
          tileSize: 256,
          maxzoom: 19,
        },
      },
      layers: [
        {
          id: "OpenStreetMap",
          type: "raster",
          source: "OpenStreetMap",
        },
      ],
    },
    zoom: 4,
    center: [-100, 40],
  },
};
