export default {
  map: {
    container: "map",
    style: "https://open.trailsta.sh/style.json",
    zoom: 4,
    center: [-100, 40],
    hash: true,
  },
  controls: [
    { type: "NavigationControl" },
    {
      type: "GeolocateControl",
      options: {
        positionOptions: {
          enableHighAccuracy: true,
        },
        trackUserLocation: true,
      },
    },
    {
      type: "FontAwesomeLinkControl",
      options: [
        "https://gitlab.com/trailstash/openstyle#opentrailstash",
        "About",
        "info",
      ],
    },
    { type: "EditOnOsmControl" },
    {
      type: "ScaleControl",
      options: {
        unit: "imperial",
      },
    },
  ],
  InitializeViewFromIP: {
    IpInfoToken: "a6134fb16df81b",
  },
};
